<?php

namespace modules\order\entities\category;

/**
 * Class Node
 *
 * @package modules\order\common\entities\category
 */
class Node
{
    /**
     * @var int
     */
    private $left = 0;

    /**
     * @var int
     */
    private $right = 0;

    /**
     * @var int
     */
    private $level = 0;

    /**
     * @return int
     */
    public function getLeft(): int
    {
        return $this->left;
    }

    /**
     * @param int $left
     *
     * @return Node
     */
    public function setLeft(int $left): Node
    {
        $this->left = $left;
        return $this;
    }

    /**
     * @return int
     */
    public function getRight(): int
    {
        return $this->right;
    }

    /**
     * @param int $right
     *
     * @return Node
     */
    public function setRight(int $right): Node
    {
        $this->right = $right;
        return $this;
    }

    /**
     * @return int
     */
    public function getLevel(): int
    {
        return $this->level;
    }

    /**
     * @param int $level
     *
     * @return Node
     */
    public function setLevel(int $level): Node
    {
        $this->level = $level;
        return $this;
    }
}
