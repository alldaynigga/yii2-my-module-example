<?php

namespace modules\order\entities\category;

/**
 * Class Category
 *
 * @package modules\order\common\entities\category
 */
class Category
{
    /**
     * @var
     */
    private $id;

    /**
     * @var int
     */
    private $parent_id = 0;

    /**
     * @var string
     */
    private $title = "";

    /**
     * @var string
     */
    private $slug = "";

    /**
     * @var int
     */
    private $level = 0;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return Category
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getParentId(): int
    {
        return $this->parent_id;
    }

    /**
     * @param int $parent_id
     *
     * @return Category
     */
    public function setParentId(int $parent_id): Category
    {
        $this->parent_id = $parent_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return Category
     */
    public function setTitle(string $title): Category
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     *
     * @return Category
     */
    public function setSlug(string $slug): Category
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return int
     */
    public function getLevel(): int
    {
        return $this->level;
    }

    /**
     * @param int $level
     *
     * @return Category
     */
    public function setLevel(int $level): Category
    {
        $this->level = $level;
        return $this;
    }
}
