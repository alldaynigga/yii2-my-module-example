<?php

namespace modules\order\entities;

/**
 * Class Delivery
 * @package modules\order\entities
 */
class Delivery
{
    /**
     * @var int $methodId
     */
    private $methodId = 0;

    /**
     * @var string $zip
     */
    private $zip = "";

    /**
     * @var string $city
     */
    private $city = "";

    /**
     * @return int
     */
    public function getMethodId(): int
    {
        return $this->methodId;
    }

    /**
     * @param int $methodId
     * @return $this
     */
    public function setMethodId(int $methodId): Delivery
    {
        $this->methodId = $methodId;

        return $this;
    }

    /**
     * @return string
     */
    public function getZip(): string
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     * @return $this
     */
    public function setZip(string $zip): Delivery
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return $this
     */
    public function setCity(string $city): Delivery
    {
        $this->city = $city;

        return $this;
    }
}
