<?php

namespace modules\order\entities;

use Ramsey\Uuid\UuidInterface;

/**
 * Class Feature
 *
 * @package modules\order\entities
 */
class Feature
{
    /**
     * @var UuidInterface
     */
    private $guid;

    /**
     * @var string
     */
    private $value = "";

    /**
     * @var string
     */
    private $name = "";

    /**
     * Feature constructor.
     *
     * @param UuidInterface $guid
     * @param string $value
     * @param string $name
     */
    public function __construct(UuidInterface $guid, string $value, string $name)
    {
        $this->guid = $guid;
        $this->value = $value;
        $this->name = $name;
    }

    /**
     * @return UuidInterface
     */
    public function getGuid(): UuidInterface
    {
        return $this->guid;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $res = json_encode([
            "guid" => $this->getGuid()->toString(),
            "name" => $this->getName(),
            "value" => $this->getValue(),
        ]);

        return $res === false ? "" : $res;
    }
}
