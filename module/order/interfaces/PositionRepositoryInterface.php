<?php

namespace modules\order\interfaces;

use modules\order\entities\Position;

/**
 * Interface PositionRepositoryInterface
 *
 * @package modules\order
 */
interface PositionRepositoryInterface
{
    /**
     * Проучение имени репозитория
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Получение списка всех позиций
     *
     * @param string[]|int[] $ids
     *
     * @param string $orderId
     *
     * @return Position[]
     */
    public function allPositionsById(array $ids, string $orderId): array;

    /**
     *
     * @return bool
     */
    public function isProductInfoSourceEnabled(): bool;

    /**
     * Возвращает позиции с дополнительными данными, которые может извлечь
     *
     * Позиции возвращаются новые, данные из старых в них не копируются.
     *
     * @param Position[] $positions
     * @param string $orderID
     *
     * @return Position[]
     */
    public function expandPositions(array $positions, string $orderID): array;
}
