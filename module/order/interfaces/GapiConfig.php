<?php

namespace modules\order\interfaces;

/**
 * Interface GapiConfig
 *
 * @package modules\delivery\config
 */
interface GapiConfig
{
    /**
     * @param int $value
     *
     * @return mixed
     */
    public function setProductFeatureTimeout(int $value);

    /**
     * @return mixed
     */
    public function getProductFeatureTimeout();
}
