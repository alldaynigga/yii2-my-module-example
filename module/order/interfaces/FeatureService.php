<?php

namespace modules\order\interfaces;

use modules\order\entities\Feature;
use modules\order\exceptions\UnableToGetFeatureException;

/**
 * Interface FeatureService
 *
 * @package modules\order\interfaces
 */
interface FeatureService
{
    /**
     * @return Feature[]
     * @throws UnableToGetFeatureException
     */
    public function getFeatures(): array;
}
