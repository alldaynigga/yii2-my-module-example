<?php

namespace modules\order\interfaces;

use modules\order\entities\Customer;
use modules\order\exceptions\UnableToGetCustomerException;

/**
 * Interface CustomerRepositoryInterface
 *
 * @package modules\order\interfaces
 */
interface CustomerRepositoryInterface
{
    /**
     * Получение информации о покупателе по идентификатору заказа
     *
     * @param string $orderID
     *
     * @return Customer
     * @throws UnableToGetCustomerException
     */
    public function getByOrderID(string $orderID): Customer;
}
