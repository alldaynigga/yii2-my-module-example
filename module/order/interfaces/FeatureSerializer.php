<?php

namespace modules\order\interfaces;

use modules\order\entities\Feature;

/**
 * Interface FeatureSerializer
 *
 * @package modules\order\interfaces
 */
interface FeatureSerializer
{
    /**
     * @param Feature $feature
     *
     * @return array
     */
    public function toArray(Feature $feature): array;

    /**
     * @param Feature[] $data
     *
     * @return array
     */
    public function serialize(array $data): array;
}
