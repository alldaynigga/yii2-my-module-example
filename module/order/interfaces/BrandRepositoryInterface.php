<?php

namespace modules\order\interfaces;

use modules\order\entities\Brand;
use modules\order\filters\BrandsFilter;

/**
 * Interface BrandRepositoryInterface
 * @package modules\order\interfaces
 */
interface BrandRepositoryInterface
{
    /**
     * @param BrandsFilter $filter
     * @return Brand[]
     */
    public function all(BrandsFilter $filter): array;
}
