<?php
declare(strict_types=1);

namespace modules\order\repositories;

use GenericApiThrift\V1\Np\Brand\BrandDto;
use GenericApiThrift\V1\Np\Brand\BrandFilterDto;
use GenericApiThrift\V1\Np\Brand\BrandServiceIf;
use modules\log\ContextBuilder;
use modules\log\OperationContext;
use modules\order\exceptions\UnableToGetBrandException;
use modules\order\interfaces\BrandRepositoryInterface;
use Psr\Log\LoggerInterface;
use modules\order\entities\Brand;
use modules\order\filters\BrandsFilter;

/**
 * Class BrandGapiRepository
 * @package modules\order\repositories
 */
class BrandGapiRepository implements BrandRepositoryInterface
{
    private $brandService;

    private $logger;

    /**
     * BrandGapiRepository constructor.
     * @param BrandServiceIf $brandService
     * @param LoggerInterface $logger
     */
    public function __construct(BrandServiceIf $brandService, LoggerInterface $logger)
    {
        $this->brandService = $brandService;
        $this->logger = $logger;
    }

    /**
     * @param BrandsFilter $filter
     * @return Brand[]
     */
    public function all(BrandsFilter $filter): array
    {
        try {
            $brandFilter = $this->makeBrandFilterDto($filter);

            $op = new OperationContext("Запрос в gAPI для получения списка брендов");
            $brands = $this->brandService->getBrandList($brandFilter);
            $op->stop();

            $logCtx = (new ContextBuilder())->setCategoryCtx(static::class)->setOperation($op)->build();
            $this->logger->info("Получение списка брендов из gAPI", $logCtx);

            return $this->dtoToBrands($brands);
        } catch (\Exception $e) {
            throw new UnableToGetBrandException("Не удалось получить список брендов", $e->getCode(), $e);
        }
    }

    /**
     * @param BrandsFilter $filter
     * @return BrandFilterDto
     */
    private function makeBrandFilterDto(BrandsFilter $filter): BrandFilterDto
    {
        return new BrandFilterDto([
            "brandStartString" => $filter->getName()
        ]);
    }

    /**
     * @param BrandDto[] $brandDtos
     * @return Brand[]
     */
    private function dtoToBrands(array $brandDtos): array
    {
        $brands = [];

        foreach ($brandDtos as $brandDto) {
            $brands[] = new Brand("", $brandDto->getTitle(), "");
        }

        return $brands;
    }
}
