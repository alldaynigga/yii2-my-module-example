<?php

namespace modules\order\repositories;

use modules\db\Connection;
use modules\order\entities\category\Category as EntityCategory;
use yii\db\Exception;
use yii\db\Query;

/**
 * Class Category
 *
 * @package modules\order\repositories
 */
class Category
{
    /**
     * @var Connection
     */
    private $db;

    /**
     * Category constructor.
     *
     * @param Connection $db
     */
    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    /**
     * @param string $title
     *
     * @return EntityCategory[]
     * @throws Exception
     */
    public function all($title = ""): array
    {
        $result = [];

        $items = [];

        if (empty($title)) {
            $items[] = "";
        } else {
            $items = $this->getNodeData($title);
        }

        foreach ($items as $item) {
            if (empty($title)) {
                $rows = $this->getAll();
            } else {
                $rows = $this->getBranchUp($item["left"], $item["right"]);
            }
            foreach ($rows as $row) {
                $result[$row["id"]] = $this->toEntity($row);
            }
        }

        return array_values($result);
    }

    /**
     * @param int $left
     * @param int $right
     *
     * @return array
     * @throws Exception
     */
    private function getBranchUp(int $left, int $right)
    {
        $sql = "SELECT * FROM category WHERE `left` <= :left AND `right` >= :right ORDER BY level ASC";
        $command = $this->db->createCommand($sql, [
            ":left" => $left,
            ":right" => $right,
        ]);

        return $command->queryAll();
    }

    /**
     * @param int $id
     * @param string $title
     * @param string $slug
     * @param int $parentId
     *
     * @return string
     * @throws Exception
     */
    public function add(int $id, string $title = "", string $slug = "", int $parentId = 0)
    {
        $query = "select `right`, level from category where id=:id";
        $command = $this->db->createCommand($query, [
            ":id" => $parentId,
        ]);

        $parent = $command->queryOne();

        $parent["right"] = $parent["right"] ?? 0;

        $parent["level"] = $parent["level"] ?? 0;

        $this->resizeAt($parent["right"] - 1, 2);

        $values = [
            "id" => $id,
            "title" => $title,
            "slug" => $slug,
            "parent_id" => $parentId,
            "left" => $parent["right"] ?? 0,
            "right" => $parent["right"] + 1,
            "level" => empty($parentId) ? 0 : $parent["level"] + 1,
        ];

        $this->db->createCommand()->insert(
            "category",
            $values
        )->execute();

        return true;
    }

    /**
     * @param int $position
     * @param int $value
     *
     * @return $this
     * @throws Exception
     */
    protected function resizeAt(int $position, int $value): self
    {
        $this->db->createCommand("UPDATE category SET `left`=`left`+:val WHERE `left` > :pos", [
            ":val" => $value,
            ":pos" => $position,
        ])->execute();

        $this->db->createCommand("UPDATE category SET `right`=`right`+:val WHERE `right` > :pos", [
            ":val" => $value,
            ":pos" => $position,
        ])->execute();

        return $this;
    }

    /**
     * @return bool
     */
    public function truncate()
    {
        try {
            (new Query())->createCommand($this->db)->setSql("truncate category")->execute();
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param array $data
     *
     * @return EntityCategory
     */
    private function toEntity(array $data)
    {
        return (new EntityCategory())
            ->setId($data["id"])
            ->setTitle($data["title"])
            ->setSlug($data["slug"])
            ->setParentId($data["parent_id"])
            ->setLevel($data["level"]);
    }

    /**
     * @param string $title
     *
     * @return array
     * @throws Exception
     */
    private function getNodeData(string $title): array
    {
        return $this->db->createCommand(
            "SELECT *  FROM `category` WHERE  title like :title",
            [
                ":title" => "%{$title}%",
            ]
        )->queryAll();
    }

    /**
     * @return array
     * @throws Exception
     */
    private function getAll()
    {
        return $this->db
            ->createCommand("SELECT *  FROM `category` order by level ASC")
            ->queryAll();
    }
}
