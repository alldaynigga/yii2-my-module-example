<?php

namespace modules\order\repositories;

use modules\log\ContextBuilder;
use modules\log\OperationContext;
use modules\order\constants\EnumProductInfoConfig;
use modules\order\entities\Feature;
use modules\order\entities\Position;
use modules\order\exceptions\UnableToGetFeatureException;
use modules\order\interfaces\PositionRepositoryInterface;
use modules\order\interfaces\PositionServiceConfig;
use modules\order\services\FeatureService;
use Psr\Log\LoggerInterface;

/**
 * Class FeatureGAPIPositionRepository
 *
 * @package modules\order\repositories
 */
class FeatureGAPIPositionRepository extends ExternalPosition implements PositionRepositoryInterface
{
    /**
     * @var FeatureService
     */
    private $featureService;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * FeatureGAPIPositionRepository constructor.
     *
     * @param FeatureService $featureService
     * @param PositionServiceConfig $productInfoSource
     * @param LoggerInterface $logger
     */
    public function __construct(FeatureService $featureService, PositionServiceConfig $productInfoSource, LoggerInterface $logger)
    {
        $this->featureService = $featureService;
        $this->logger = $logger;
        $this->productInfoSource = $productInfoSource;

        parent::__construct($logger);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return EnumProductInfoConfig::GAPI;
    }

    /**
     * @inheritDoc
     */
    protected function getPositionIdsToFill(array $positions): array
    {
        $toFill = [];

        // Проиндексируем позиции и запишем ID тех позиций, инфорацию по
        // которым нужно дополнительно запросить из источника данных по позициям
        foreach ($positions as $position) {
            $needToFeelFeatures = !$position->areFeaturesDirty() && empty($position->getFeatures());

            if ($needToFeelFeatures) {
                $this->logger->info(sprintf(
                    "По позиции %s недостаточно данных по фичам. Будем запрашивать их дополнительно",
                    $position->getId()
                ), (new ContextBuilder())
                    ->setCategoryCtx(static::class)
                    ->build());

                $toFill[] = $position->getId();
            }
        }

        return $toFill;
    }

    /**
     * @param string[] $ids
     * @param string $orderId
     *
     * @return Position[]
     */
    public function allPositionsById(array $ids, string $orderId): array
    {
        $positions = [];
        $rawProductsData = $this->featureService->getDeliveryRestrictionBySkuIds($ids);
        foreach ($ids as $id) {
            if (!array_key_exists($id, $rawProductsData)) {
                $positions[] = (new Position())->setId($id);
                continue;
            }
            $features = $rawProductsData[$id];
            $position = new Position();
            $position->setId($id);
            $position->setFeatures($features);
            $positions[] = $position;
        }

        return $positions;
    }
}
