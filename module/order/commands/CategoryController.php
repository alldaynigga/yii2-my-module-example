<?php

namespace modules\order\commands;

use modules\order\commands\category\FillAction;
use yii\console\Controller;

/**
 * Class CategoryController
 *
 * @package modules\order\commands
 */
class CategoryController extends Controller
{
    /**
     * @return array
     */
    public function actions()
    {
        return [
            "fill" => FillAction::class,
        ];
    }
}
