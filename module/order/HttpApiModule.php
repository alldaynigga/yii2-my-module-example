<?php

namespace modules\order;

use modules\order\web\backend\controllers\CustomersController;
use modules\order\web\backend\controllers\OrderTypesController;
use modules\order\web\backend\controllers\PositionController;
use yii\base\Module as BaseModule;

/**
 * Class Module
 *
 * @package modules\order
 */
class HttpApiModule extends BaseModule
{
    public $controllerMap = [
        "position" => PositionController::class,
        "order-types" => OrderTypesController::class,
        "customers" => CustomersController::class
    ];
}
