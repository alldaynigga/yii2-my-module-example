<?php

namespace modules\order\constants;

use modules\core\common\constants\Base;

/**
 * Class OrderType
 *
 * @package modules\order\constants
 */
class OrderType extends Base
{
    /**
     * не указан (по-умолчанию)
     */
    public const UNDEFINED = "undefined";
    /**
     * обычный заказ
     */
    public const ITEM = "item";

    /**
     *  предзаказ
     */
    public const PREORDER = "preorder";

    /**
     *  подарочная карта
     */
    public const GIFTCARD = "giftcard";

    /**
     *  виртуальная подарочная карта
     */
    public const VIRTUAL_GIFTCARD = "virtual-giftcard";

    /**
     *  ювелирка с самовывоз
     */
    public const JEWELRY_PICKUP = "jewelry";

    /**
     *  ювелирка с курьером
     */
    public const  JEWELRY_COURIER = "jewelry-buy";

    /**
     *  быстрый заказ
     */
    public const QUICK = "quick";

    /**
     *  смешанный заказ
     */
    public const MIXED = "mixed";

    /**
     * @return array
     */
    public static function getLabels()
    {
        return [
            static::UNDEFINED => "не указан",
            static::ITEM => "обычный заказ",
            static::PREORDER => "предзаказ",
            static::GIFTCARD => "подарочная карта",
            static::VIRTUAL_GIFTCARD => "виртуальная подарочная карта",
            static::JEWELRY_PICKUP => "ювелирка",
            static::JEWELRY_COURIER => "ювелирка с курьером",
            static::QUICK => "быстрый заказ",
            static::MIXED => "смешанный заказ",
        ];
    }
}
