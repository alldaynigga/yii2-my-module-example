<?php

namespace modules\order\constants;

use modules\core\common\constants\Base;

/**
 * Class EnumProductInfoConfig
 *
 * @package modules\config\constants
 */
class EnumProductInfoConfig extends Base
{
    /**
     * Источник NP
     */
    public const NP = "np";

    /**
     * Источник GEO
     */
    public const GAPI = "gapi";

    /**
     * @return array
     */
    public static function getLabels()
    {
        return [
            self::NP => "NP",
            self::GAPI => "gAPI",
        ];
    }
}
