<?php

namespace modules\order\serializers;

use modules\order\entities\category\Category;

/**
 * Class CategorySerializer
 *
 * @package modules\order\serializers
 */
class CategorySerializer
{
    /**
     * @param Category $category
     *
     * @return array
     */
    public function toArray(Category $category): array
    {
        return [
            "id" => $category->getId(),
            "title" => $category->getTitle(),
            "slug" => $category->getSlug(),
            "level" => $category->getLevel(),
            "parent_id" => !empty($category->getParentId()) ? $category->getParentId() : null,
        ];
    }

    /**
     * @param Category[] $data
     *
     * @return array
     */
    public function serialize(array $data): array
    {
        $result = [];

        foreach ($data as $entity) {
            $result[] = $this->toArray($entity);
        }

        return $result;
    }
}
