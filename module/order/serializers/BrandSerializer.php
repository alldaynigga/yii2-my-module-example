<?php
declare(strict_types=1);

namespace modules\order\serializers;

use modules\order\entities\Brand;
use modules\order\interfaces\BrandSerializerInterface;

/**
 * Class BrandSerializer
 * @package modules\order\serializers
 */
class BrandSerializer implements BrandSerializerInterface
{
    /**
     * @param Brand[] $brands
     * @return array
     */
    public function serialize(array $brands): array
    {
        $res = [];

        foreach ($brands as $brand) {
            $res[] = $this->toArray($brand);
        }

        return $res;
    }

    /**
     * @param Brand $brand
     * @return array
     */
    public function toArray(Brand $brand): array
    {
        return [
            "id"   => $brand->getId(),
            "name" => $brand->getName(),
            "slug" => $brand->getSlug()
        ];
    }
}
