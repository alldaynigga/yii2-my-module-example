<?php

namespace modules\order\exceptions;

/**
 * Class UnableToGetFeatureException
 *
 * @package modules\order\exceptions
 */
class UnableToGetBrandException extends OrderException
{
}
