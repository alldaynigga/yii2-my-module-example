<?php

namespace modules\order\exceptions;

use DomainException;

/**
 * Class OrderException
 *
 * @package modules\order\exceptions
 */
class OrderException extends DomainException
{

}
