<?php

namespace modules\order\exceptions;

/**
 * Class InvalidCustomerRoleException
 *
 * @package modules\order\exceptions
 */
class InvalidCustomerRoleException extends OrderException
{

}
