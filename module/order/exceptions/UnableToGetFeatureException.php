<?php

namespace modules\order\exceptions;

/**
 * Class UnableToGetFeatureException
 *
 * @package modules\order\exceptions
 */
class UnableToGetFeatureException extends OrderException
{
}
