<?php

namespace modules\order\web\backend\controllers;

use modules\net\http\AdminController;
use modules\order\web\backend\controllers\orderTypes\ListAction;
use yii\helpers\ArrayHelper;

/**
 * Class IndexController
 *
 * @package modules\delivery\plan\web\backend\controllers
 */
class OrderTypesController extends AdminController
{
    /**
     * @return array
     */
    public function actions(): array
    {
        return ArrayHelper::merge(parent::actions(), [
            "index" => [
                "class" => ListAction::class,
            ],
        ]);
    }
}
