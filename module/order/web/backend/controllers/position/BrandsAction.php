<?php
declare(strict_types=1);

namespace modules\order\web\backend\controllers\position;

use modules\order\exceptions\UnableToGetBrandException;
use modules\order\interfaces\BrandRepositoryInterface;
use modules\order\interfaces\BrandSerializerInterface;
use modules\order\filters\BrandsFilter;
use yii\base\Action;

/**
 * Class BrandsAction
 * @package modules\order\web\backend\controllers\position
 */
class BrandsAction extends Action
{
    public $name = "";

    /** @var BrandRepositoryInterface */
    private $brandRepository;

    /** @var BrandSerializerInterface */
    private $brandSerializer;

    /**
     * BrandsAction constructor.
     * @param $id
     * @param $controller
     * @param BrandRepositoryInterface $brandRepository
     * @param BrandSerializerInterface $brandSerializer
     * @param array $config
     */
    public function __construct($id, $controller, BrandRepositoryInterface $brandRepository, BrandSerializerInterface $brandSerializer, $config = [])
    {
        $this->brandRepository = $brandRepository;
        $this->brandSerializer = $brandSerializer;
        parent::__construct($id, $controller, $config);
    }

    /**
     * @return array
     */
    public function run(): array
    {
        $res = [];

        try {
            $brandsFilter = (new BrandsFilter())->setName($this->name);
            $res = $this->brandRepository->all($brandsFilter);
        } catch (UnableToGetBrandException $e) {
            return $res;
        }

        return $this->brandSerializer->serialize($res);
    }
}
