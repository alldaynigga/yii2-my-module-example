<?php
declare(strict_types=1);

namespace modules\order\web\backend\controllers\customers;

use modules\order\constants\CustomerRole;
use yii\base\Action;

/**
 * Class CustomersAction
 * @package modules\order\web\backend\controllers\customers
 */
class CustomersAction extends Action
{
    /**
     * CustomersAction constructor.
     * @param $id
     * @param $controller
     * @param array $config
     */
    public function __construct($id, $controller, $config = [])
    {
        parent::__construct($id, $controller, $config);
    }

    /**
     * @return array
     */
    public function run()
    {
        return CustomerRole::getLabels();
    }
}
