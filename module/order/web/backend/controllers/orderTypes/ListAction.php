<?php

namespace modules\order\web\backend\controllers\orderTypes;

use modules\order\constants\OrderType;
use yii\base\Action;

/**
 * Class ListAction
 *
 * @package modules\order\web\backend\controllers\orderTypes
 */
class ListAction extends Action
{
    /**
     *
     */
    public function run()
    {
        return OrderType::getLabels();
    }
}
