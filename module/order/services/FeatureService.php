<?php

namespace modules\order\services;

use GenericApiSdkTransport\Async\PromiseWrapper;
use GenericApiThrift\V1\Content\Attribute\AttributeServiceIf;
use GenericApiThrift\V1\Content\Attribute\DeliveryRestrictionAttributeValueDto;
use GenericApiThrift\V1\Content\Exception\ContentServerConnectionException;
use GenericApiThrift\V1\Content\Exception\ContentServerErrorException;
use GenericApiThrift\V1\Exception\InternalServiceException;
use GenericApiThrift\V1\Exception\NotFoundException;
use modules\core\common\exceptions\http\InternalServerErrorException;
use modules\log\ContextBuilder;
use modules\log\OperationContext;
use modules\order\entities\Feature;
use modules\order\exceptions\UnableToGetFeatureException;
use modules\order\interfaces\FeatureService as FeatureServiceInterface;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Throwable;
use GenericApiSdk\V1\Content\Model\ModelServiceClientAsync;

/**
 * Class FeatureService
 *
 * @package modules\order\position\services
 */
class FeatureService implements FeatureServiceInterface
{
    /**
     * @var AttributeServiceIf
     */
    private $attributeService;

    /**
     * @var ModelServiceClientAsync
     */
    private $modelServiceClient;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var bool
     */
    private $isGapiRequestsLoggingEnabled;

    /**
     * FeatureService constructor.
     *
     * @param AttributeServiceIf $attributeService
     * @param ModelServiceClientAsync $modelServiceClient
     * @param LoggerInterface $logger
     * @param bool $isGapiRequestsLoggingEnabled
     */
    public function __construct(AttributeServiceIf $attributeService, ModelServiceClientAsync $modelServiceClient, LoggerInterface $logger, bool $isGapiRequestsLoggingEnabled)
    {
        $this->attributeService = $attributeService;
        $this->modelServiceClient = $modelServiceClient;
        $this->logger = $logger;
        $this->isGapiRequestsLoggingEnabled = $isGapiRequestsLoggingEnabled;
    }

    /**
     * @return Feature[]
     * @throws UnableToGetFeatureException
     */
    public function getFeatures(): array
    {
        $features = [];
        try {
            $data = $this->attributeService->getDeliveryRestrictionList();

            if ($this->isGapiRequestsLoggingEnabled) {
                $str = json_encode($data, JSON_UNESCAPED_UNICODE);
                $this->logger->info($str, (new ContextBuilder())
                    ->setCategoryCtx(static::class)
                    ->setTopic("Получение фич от gAPI.")
                    ->build());
            }

            foreach ($data as $item) {
                $features[] = new Feature(Uuid::fromString($item->getGuid()), $item->getValue(), $item->getName());
            }
        } catch (ContentServerConnectionException $e) {
            throw new UnableToGetFeatureException($e->getMessage(), $e->getCode(), $e);
        } catch (ContentServerErrorException $e) {
            throw new UnableToGetFeatureException($e->getMessage(), $e->getCode(), $e);
        } catch (InternalServiceException $e) {
            throw new UnableToGetFeatureException($e->getMessage(), $e->getCode(), $e);
        } catch (Throwable $exception) {
            $this->logger->error("Навернулся при получении данных от gAPI.", (new ContextBuilder())
                ->setCategoryCtx(static::class)
                //Если установить топик то логер не правильно формирует сообщение для кибаны
                ->setTopic("Навернулся при получении данных от gAPI.")
                ->setException($exception)
                ->build());

            throw new UnableToGetFeatureException($exception->getMessage(), $exception->getCode(), $exception);
        }

        return $features;
    }

    /**
     * @param array $skuIds
     * @return Feature[]
     */
    public function getDeliveryRestrictionBySkuIds(array $skuIds): array
    {
        $result = [];
        $promises = [];
        $promiseWrapper = new PromiseWrapper();

        foreach ($skuIds as $skuId) {
            if ($this->isGapiRequestsLoggingEnabled) {
                $this->logger->info("Запрос ограничений для SKU = $skuId от gAPI.", (new ContextBuilder())
                    ->setCategoryCtx(static::class)
                    ->setTopic("Получение ограничений для SKU от GAPI")
                    ->build());
            }

            $promise = $this->modelServiceClient->getDeliveryRestrictionBySkuId($skuId);
            $promiseWrapper->add($promise);
            $promises[$skuId] = $promise;
        }

        $promiseWrapper->waitAll();

        foreach ($promises as $skuId => $promise) {
            $opGAPI = new OperationContext("Получение информации об особенностях доставки товара от gAPI");
            try {
                $data = $promise->resolve();

                $this->logGetDeliveryRestriction($skuId, $data);

                $result[$skuId] = $this->makeFeatures($data);
            } catch (NotFoundException $e) {
                $opGAPI->stop()->failed();

                $message = sprintf("При попытке получить информацию об особенностях доставки товара %s gAPI сказал, что данных нет", $skuId);

                $this->logger->info($message, (new ContextBuilder())
                    ->setCategoryCtx(static::class)
                    ->setOperation($opGAPI)
                    ->setException($e)
                    ->build());
                continue;
            } catch (InternalServiceException $e) {
                $opGAPI->stop()->failed();
                $message = sprintf(
                    "При попытке получить информацию об особенностях доставки товара %s словили ошибку",
                    $skuId
                );

                $this->logger->warning($message, (new ContextBuilder())
                    ->setCategoryCtx(static::class)
                    ->setOperation($opGAPI)
                    ->setException($e)
                    ->setTopic("Не удалось получить информацию об особенностях доставки от gAPI")
                    ->build());
                continue;
            } catch (\Throwable $e) {
                $this->logger->error("Навернулся при получении данных от gAPI.", (new ContextBuilder())
                    ->setCategoryCtx(static::class)
                    //Если установить топик то логер не правильно формирует сообщение для кибаны
                    ->setTopic("Навернулся при получении данных от gAPI.")
                    ->setException($e)
                    ->build());
            }

            $message = sprintf("Получили информацию об особенностях доставки товара %s от gAPI", $skuId);
            $opGAPI->stop();
            $this->logger->info($message, (new ContextBuilder())
                ->setCategoryCtx(static::class)
                ->setOperation($opGAPI)
                ->build());
        }

        return $result;
    }

    /**
     * @param string $skuId
     * @param DeliveryRestrictionAttributeValueDto[] $data
     */
    private function logGetDeliveryRestriction(string $skuId, array $data): void
    {
        if ($this->isGapiRequestsLoggingEnabled) {
            $str = json_encode(
                [
                    "Получение ограничений для SKU = $skuId от GAPI",
                    "response" => $data,
                ],
                JSON_UNESCAPED_UNICODE
            );
            $this->logger->info($str, (new ContextBuilder())
                ->setCategoryCtx(static::class)
                ->setTopic("Получение ограничений для SKU от GAPI")
                ->build());
        }
    }

    /**
     * @param DeliveryRestrictionAttributeValueDto[] $data
     * @return Feature[]
     */
    private function makeFeatures(array $data): array
    {
        $res = [];

        foreach ($data as $item) {
            if ($item instanceof DeliveryRestrictionAttributeValueDto) {
                $res[] = new Feature(
                    Uuid::fromString($item->getGuid()),
                    $item->getValue(),
                    $item->getName()
                );
            }
        }

        return $res;
    }
}
