<?php

namespace modules\partner\filters;

/**
 * Class PartnerFilter
 *
 * @package modules\partner\filters
 */
class PartnerFilter
{
    /**
     * @var string[]
     */
    private $ids = [];

    /**
     * @var string
     */
    private $name = "";

    /**
     * @return string[]
     */
    public function getIds(): array
    {
        return $this->ids;
    }

    /**
     * @param string[] $ids
     *
     * @return PartnerFilter
     */
    public function setIds(array $ids): self
    {
        $this->ids = $ids;

        return $this;
    }

    /**
     * @param string $id
     *
     * @return $this
     */
    public function addId(string $id): self
    {
        $this->ids[] = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return PartnerFilter
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
