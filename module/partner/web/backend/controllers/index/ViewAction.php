<?php

namespace modules\partner\web\backend\controllers\index;

use modules\core\common\components\web\Response;
use modules\core\common\constants\HttpStatus;
use modules\partner\exceptions\SelectOperationException;
use modules\partner\serializers\PartnerSerializer;
use modules\partner\services\Partner;
use yii\base\Action;

/**
 * Class ViewAction
 *
 * @package modules\partner\web\controllers
 */
class ViewAction extends Action
{
    /**
     * @var Partner
     */
    private $partnerService;

    /**
     * @var PartnerSerializer
     */
    private $partnerSerializer;

    /**
     * @var Response
     */
    private $response;

    /**
     * ListAction constructor.
     *
     * @param $id
     * @param $controller
     * @param Partner $partnerService
     * @param PartnerSerializer $partnerSerializer
     * @param Response $response
     * @param array $config
     */
    public function __construct($id, $controller, Partner $partnerService, PartnerSerializer $partnerSerializer, Response $response, $config = [])
    {
        parent::__construct($id, $controller, $config);
        $this->partnerService = $partnerService;
        $this->partnerSerializer = $partnerSerializer;
        $this->response = $response;
    }

    /**
     * @param string $id
     *
     * @return array
     */
    public function run(string $id)
    {
        try {
            $partner = $this->partnerService->getOneById($id);
        } catch (SelectOperationException $exception) {
            $this->response->setStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);
            return ["Не найден партнер с ID '{$id}'"];
        }

        return $this->partnerSerializer->serialize($partner);
    }
}
