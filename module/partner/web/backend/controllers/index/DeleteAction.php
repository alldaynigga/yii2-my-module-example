<?php

namespace modules\partner\web\backend\controllers\index;

use modules\core\common\constants\HttpStatus;
use modules\core\common\exceptions\http\InternalServerErrorException;
use modules\partner\exceptions\DeleteOperationException;
use modules\partner\exceptions\PartnerNotFoundException;
use modules\partner\services\Partner;
use yii\base\Action;
use yii\web\NotFoundHttpException;
use function response;

/**
 * Class DeleteAction
 *
 * @package modules\partner\web\controllers
 */
class DeleteAction extends Action
{
    /**
     * @var Partner
     */
    private $partnerService;

    /**
     * ListAction constructor.
     *
     * @param $id
     * @param $controller
     * @param Partner $partnerService
     * @param array $config
     */
    public function __construct($id, $controller, Partner $partnerService, $config = [])
    {
        parent::__construct($id, $controller, $config);
        $this->partnerService = $partnerService;
    }

    /**
     * @param string $id
     *
     * @throws InternalServerErrorException
     * @throws NotFoundHttpException
     */
    public function run(string $id)
    {
        try {
            $this->partnerService->deleteById($id);
        } catch (PartnerNotFoundException $e) {
            throw new NotFoundHttpException($e->getMessage());
        } catch (DeleteOperationException $exception) {
            throw new InternalServerErrorException("Не смог удалить партнера");
        }
        response()->setStatusCode(HttpStatus::NO_CONTENT);
    }
}
