<?php

namespace modules\partner\web\backend\controllers\index;

use modules\core\common\components\web\Response;
use modules\core\common\constants\HttpStatus;
use modules\core\common\exceptions\http\InternalServerErrorException;
use modules\core\common\exceptions\http\UnprocessableEntityException;
use modules\partner\exceptions\InvalidDataException;
use modules\partner\exceptions\PartnerNotFoundException;
use modules\partner\exceptions\UpdateOperationException;
use modules\partner\forms\PartnerForm;
use modules\partner\serializers\PartnerSerializer;
use modules\partner\services\Partner;
use yii\base\Action;

/**
 * Class UpdateAction
 *
 * @package modules\partner\web\controllers
 */
class UpdateAction extends Action
{
    /**
     * @var array
     */
    public $params = [];

    /**
     * @var Partner
     */
    private $partnerService;

    /**
     * @var PartnerSerializer
     */
    private $partnerSerializer;

    /**
     * @var Response
     */
    private $response;

    /**
     * ListAction constructor.
     *
     * @param $id
     * @param $controller
     * @param Partner $partnerService
     * @param PartnerSerializer $partnerSerializer
     * @param Response $response
     * @param array $config
     */
    public function __construct($id, $controller, Partner $partnerService, PartnerSerializer $partnerSerializer, Response $response, $config = [])
    {
        parent::__construct($id, $controller, $config);
        $this->partnerService = $partnerService;
        $this->partnerSerializer = $partnerSerializer;
        $this->response = $response;
    }

    /**
     * @param string $id
     *
     * @return array
     * @throws InternalServerErrorException
     * @throws UnprocessableEntityException
     */
    public function run(string $id)
    {
        $partnerForm = new PartnerForm();

        $partnerForm->load($this->params, "");

        $partnerForm->id = $id;

        try {
            return $this->partnerSerializer->serialize($this->partnerService->update($partnerForm));
        } catch (InvalidDataException $exception) {
            throw new UnprocessableEntityException(
                "Не удалось сохранить партнера",
                HttpStatus::UNPROCESSABLE_ENTITY,
                $exception->getErrors()
            );
        } catch (UpdateOperationException $exception) {
            if ($exception->getPrevious() instanceof PartnerNotFoundException) {
                $this->response->setStatusCode(HttpStatus::INTERNAL_SERVER_ERROR);
                throw new InternalServerErrorException("Не удалось обовить", 0, [], $exception);
            }
        }

        return [];
    }
}
