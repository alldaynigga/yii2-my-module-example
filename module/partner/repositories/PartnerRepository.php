<?php

namespace modules\partner\repositories;

use modules\core\common\exceptions\db\DuplicateEntryException;
use modules\db\Connection;
use modules\partner\entities\Partner;
use modules\partner\exceptions\DeleteOperationException;
use modules\partner\exceptions\InsertOperationException;
use modules\partner\exceptions\PartnerNotFoundException;
use modules\partner\exceptions\SelectOperationException;
use modules\partner\exceptions\UpdateOperationException;
use modules\partner\filters\PartnerFilter;
use yii\db\Exception as DbException;
use yii\db\Query;

/**
 * Class PartnerRepository
 *
 * @package modules\partner\repositories
 */
class PartnerRepository
{
    /**
     * @var Connection
     */
    private $db;

    /**
     * PartnerRepository constructor.
     *
     * @param Connection $db
     */
    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    /**
     * @param PartnerFilter $partnerFilter
     *
     * @return Query
     */
    private function buildQuery(PartnerFilter $partnerFilter): Query
    {
        $query = new Query();
        $query->from("partner");

        if (count($partnerFilter->getIds()) !== 0) {
            $query->where(["id" => $partnerFilter->getIds()]);
        }

        if (!empty($partnerFilter->getName())) {
            $query->andWhere(["like", "name", $partnerFilter->getName()]);
        }

        return $query;
    }

    /**
     * @param PartnerFilter $partnerFilter
     *
     * @return Partner
     */
    public function one(PartnerFilter $partnerFilter): Partner
    {
        $result = $this->buildQuery($partnerFilter)->one($this->db);

        if ($result === false) {
            throw new SelectOperationException(
                "Ошибка при выборке",
                SelectOperationException::ERROR_CODE,
                new PartnerNotFoundException("Не найден партнер по ID", PartnerNotFoundException::ERROR_CODE_FIND)
            );
        }

        return $this->toEntity($result);
    }

    /**
     * @param PartnerFilter $partnerFilter
     *
     * @return Partner[]
     * @throws PartnerNotFoundException
     */
    public function all(PartnerFilter $partnerFilter): array
    {
        $all = $this->buildQuery($partnerFilter)->all($this->db);

        return array_map(function ($row) {
            return $this->toEntity($row);
        }, $all);
    }

    /**
     * Получение количества партнеров по фильтру
     *
     * @param PartnerFilter $partnerFilter
     *
     * @return int
     */
    public function count(PartnerFilter $partnerFilter): int
    {
        return $this->buildQuery($partnerFilter)->count();
    }

    /**
     * @param Partner $partner
     *
     * @return bool
     */
    public function insert(Partner $partner): bool
    {
        $data = [
            "id" => $partner->getId(),
            "name" => $partner->getName(),
        ];

        try {
            return $this->db->createCommand()->insert("partner", $data)->execute() > 0;
        } catch (DbException $exception) {
            if ($exception instanceof DuplicateEntryException) {
                throw new InsertOperationException("Ошибка при вставке дублирование записи", InsertOperationException::ERROR_DUPLICATE_ENTRY, $exception);
            }

            throw new InsertOperationException("Ошибка при вставке", InsertOperationException::ERROR_CODE_DB, $exception);
        }
    }

    /**
     * @param Partner $partner
     *
     * @return bool
     */
    public function update(Partner $partner): bool
    {
        $data = [
            "name" => $partner->getName(),
        ];

        try {
            $this->db->createCommand()->update("partner", $data, "id = :id", [":id" => $partner->getId()])->execute();
        } catch (DbException $e) {
            throw new UpdateOperationException("Ошибка при обновлении", UpdateOperationException::ERROR_CODE_DB, $e);
        }

        return true;
    }

    /**
     * @param string $id
     *
     * @return bool
     * @throws DbException
     * @throws PartnerNotFoundException
     */
    public function deleteById(string $id): bool
    {
        $filter = (new PartnerFilter())->setIds([$id]);
        if ($this->count($filter) == 0) {
            throw new PartnerNotFoundException(sprintf("Не удалось найти партнера %s", $id));
        }

        $result = $this->db
                ->createCommand()
                ->delete("partner", "id = :id", [":id" => $id])
                ->execute() > 0;

        if (!$result) {
            throw new DeleteOperationException(
                "Ошибка при удалении",
                DeleteOperationException::ERROR_CODE_DB,
                new PartnerNotFoundException("Не найден партнер для удаления", PartnerNotFoundException::ERROR_CODE_DELETE)
            );
        }

        return $result;
    }

    /**
     * @param array $row
     *
     * @return Partner
     */
    public function toEntity(array $row): Partner
    {
        return (new Partner($row["id"], $row["name"]));
    }

    /**
     * TODO: подпорка надо от нее избавится
     *
     * @param string $key
     * @param PartnerFilter $partnerFilter
     *
     * @return Partner[]
     */
    public function indexByAll(string $key, PartnerFilter $partnerFilter): array
    {
        $rows = $this->buildQuery($partnerFilter)->all($this->db);

        $data = [];
        foreach ($rows as $row) {
            $data[$row[$key]] = $this->toEntity($row);
        }

        return $data;
    }
}
