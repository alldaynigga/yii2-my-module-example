<?php

namespace modules\partner\exceptions;

/**
 * Class UpdateOperationException
 *
 * @package modules\partner\exceptions
 */
class UpdateOperationException extends PartnerException
{
    public const ERROR_CODE = 1;

    public const ERROR_CODE_DB = 2;
}
