<?php

namespace modules\partner\exceptions;

/**
 * Class InsertOperationException
 *
 * @package modules\partner\exceptions
 */
class SaveOperationException extends PartnerException
{
}
