<?php

namespace modules\partner\services;

use modules\log\ContextBuilder;
use modules\partner\entities\Partner as EntityPartner;
use modules\partner\exceptions\InvalidDataException;
use modules\partner\exceptions\SelectOperationException;
use modules\partner\filters\PartnerFilter;
use modules\partner\forms\PartnerForm;
use modules\partner\repositories\PartnerRepository;
use Psr\Log\LoggerInterface;
use yii\db\Exception;

/**
 * Class Partner
 *
 * @package modules\partner\services
 */
class Partner
{
    /**
     * @var PartnerRepository
     */
    private $repository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Partner constructor.
     *
     * @param PartnerRepository $repository
     * @param LoggerInterface $logger
     */
    public function __construct(PartnerRepository $repository, LoggerInterface $logger)
    {
        $this->repository = $repository;
        $this->logger = $logger;
    }

    /**
     * @param string $id
     *
     * @return EntityPartner
     * @throws SelectOperationException
     */
    public function getOneById(string $id): EntityPartner
    {
        try {
            return $this->repository->one((new PartnerFilter())->setIds([$id]));
        } catch (SelectOperationException $e) {
            $this->logger->error($e, (new ContextBuilder())->setCategoryCtx(static::class)->build());
            throw $e;
        }
    }

    /**
     * @param PartnerForm $partnerForm
     *
     * @return EntityPartner
     */
    public function insert(PartnerForm $partnerForm): EntityPartner
    {
        if (!$partnerForm->validate() || !$this->validateCreate($partnerForm)) {
            throw (new InvalidDataException())
                ->setErrors($partnerForm->getErrors());
        }

        $partner = $this->buildEntity($partnerForm);

        $this->repository->insert($partner);

        return $partner;
    }

    /**
     * @param PartnerForm $partnerForm
     *
     * @return EntityPartner
     */
    public function update(PartnerForm $partnerForm): EntityPartner
    {
        if (!$partnerForm->validate() || !$this->validateUpdate($partnerForm)) {
            throw (new InvalidDataException())
                ->setErrors($partnerForm->getErrors());
        }

        $partner = $this->buildEntity($partnerForm);

        $this->repository->update($partner);

        return $partner;
    }

    /**
     * @param PartnerFilter $partnerFilter
     *
     * @return EntityPartner[]
     */
    public function getAll(PartnerFilter $partnerFilter): array
    {
        return $this->repository->all($partnerFilter);
    }

    /**
     * @param string $id
     *
     * @return bool
     * @throws Exception
     */
    public function deleteById(string $id): bool
    {
        try {
            $this->repository->deleteById($id);
        } catch (Exception $e) {
            $this->logger->error($e, (new ContextBuilder())->setCategoryCtx(static::class)->build());
            throw $e;
        }

        return true;
    }

    /**
     * @param PartnerForm $partnerForm
     *
     * @return PartnerFilter
     */
    public function buildFilter(PartnerForm $partnerForm)
    {
        $partnerFilter = new PartnerFilter();

        if (!empty($partnerForm->id)) {
            $partnerFilter->setIds($partnerForm->id);
        }

        if (!empty($partnerForm->name)) {
            $partnerFilter->setName($partnerForm->name);
        }

        return $partnerFilter;
    }

    /**
     * @param PartnerForm $partnerForm
     *
     * @return EntityPartner
     */
    private function buildEntity(PartnerForm $partnerForm): EntityPartner
    {
        return (new EntityPartner($partnerForm->id, $partnerForm->name));
    }

    /**
     * @param PartnerForm $partnerForm
     *
     * @return bool
     */
    private function validateCreate(PartnerForm $partnerForm): bool
    {
        try {
            $this->getOneById($partnerForm->id);
        } catch (SelectOperationException $exception) {
            return true;
        }

        $partnerForm->addError("id", "Значение должно быть уникальным");

        return false;
    }

    /**
     * @param PartnerForm $partnerForm
     *
     * @return bool
     */
    private function validateUpdate(PartnerForm $partnerForm): bool
    {
        try {
            $this->getOneById($partnerForm->id);
        } catch (SelectOperationException $exception) {
            $partnerForm->addError("id", "Не найден партнер с ID " . $partnerForm->id);
            return false;
        }

        return true;
    }
}
