<?php

namespace modules\partner\entities;

/**
 * Class Partner
 *
 * @package modules\partner\entities
 */
class Partner
{
    /**
     * @var string
     */
    private $id = "";

    /**
     * @var string
     */
    private $name = "";

    /**
     * Partner constructor.
     *
     * @param string $id
     * @param string $name
     */
    public function __construct(string $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
