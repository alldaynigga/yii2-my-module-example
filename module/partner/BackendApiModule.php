<?php

namespace modules\partner;

use modules\partner\web\backend\controllers\IndexController;
use yii\base\Module;

/**
 * Class BackendApiModule
 *
 * @package modules\partner
 */
class BackendApiModule extends Module
{
    public $controllerMap = [
        "partner" => IndexController::class,
    ];
}
